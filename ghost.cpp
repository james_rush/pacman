#include "ghost.h"
#include "moveableitem.h"
#include "pacman.h"
#include <QGraphicsScene>
#include <QTime>

ghost::ghost(QGraphicsItem* parent,int x,int y,QString picpath,QGraphicsRectItem *w[31][28] ,int *deadG,int* deadP,pacman* pac):moveableitem(parent){
    this->setPixmap(QPixmap(picpath).scaled(QSize(13,13)));
    this->setPos(2 + 15 * x,2 + 15 * y);
    this->GhostX = x;
    this->GhostY = y;
    this->originalX = x;
    this->originalY = y;
    for (int i = 0;i < 31;++i) {//copy wall array
        for(int j = 0;j < 28 ;++j){
            this->wall[i][j] = w[i][j];
        }

    }
    //copy all needed arguments for mainwindow
    this->deadGhost = deadG;
    this->deadPacman = deadP;
    this->pac = pac;

    qsrand(static_cast<uint>(QTime::currentTime().msec()));//set random seed
    tmr = new QTimer;
    tmr->start(20);
    dir = new QTimer;
    revive = new QTimer;
    connect(revive,SIGNAL(timeout()),this,SLOT(reviveGhost()));
}
ghost::~ghost(){

}

void ghost::move(int mode){
    switch (mode) {
        case 0:
            if(this->dead == false){
                movePatternA();
            }
        break;
        case 1:
            if(this->dead == false){
                movePatternB();
            }
        break;
        case 2:
            if(this->dead == false){
                movePatternC();
            }
        break;
        case 3:
            if(this->dead == false){
                movePatternD();
            }
        break;
    }
}

void ghost::movePatternA(){//red move pattern
    dir->start(5000);//change direction every 5 secs
    if(currentDirection == 4){
        currentDirection = 0;//red move down first
        nextDirection = 2;
    }
    switch (currentDirection) {//signal and slot control direction
        case 0://down
            if(wall[this->GhostY + 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){//check turn able and turn at center of blocks
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveDown()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 1://up
            if(wall[this->GhostY - 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveUp()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 2://right
            if(wall[this->GhostY][this->GhostX + 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveRight()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 3://left
            if(wall[this->GhostY][this->GhostX - 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveLeft()));
            }
            else {
                forceToChangeDirection();
            }
        break;
    }
}

void ghost::movePatternB(){//pink move pattern
    dir->start(7000);//change direction every 7 secs
    if(currentDirection == 4){
        currentDirection = 3;//pink move left first
        nextDirection = 1;
    }
    switch (currentDirection) {//signal and slot control direction
        case 0://down
            if(wall[this->GhostY + 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){//check turn able and turn at center of blocks
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveDown()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 1://up
            if(wall[this->GhostY - 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveUp()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 2://right
            if(wall[this->GhostY][this->GhostX + 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveRight()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 3://left
            if(wall[this->GhostY][this->GhostX - 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveLeft()));
            }
            else {
                forceToChangeDirection();
            }
        break;
    }
}

void ghost::movePatternC(){//blue move pattern
    dir->start(9000);//change direction every 9 secs
    if(currentDirection == 4){
        currentDirection = 2;//blue move right first
        nextDirection = 0;
    }
    switch (currentDirection) {//signal and slot control direction
        case 0://down
            if(wall[this->GhostY + 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){//check turn able and turn at center of blocks
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveDown()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 1://up
            if(wall[this->GhostY - 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveUp()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 2://right
            if(wall[this->GhostY][this->GhostX + 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveRight()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 3://left
            if(wall[this->GhostY][this->GhostX - 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveLeft()));
            }
            else {
                forceToChangeDirection();
            }
        break;
    }
}

void ghost::movePatternD(){//yellow move pattern
    dir->start(11000);//changedir every 11 secs
    if(currentDirection == 4){
        currentDirection = 1;//yellow move up first
        nextDirection = 3;
    }
    switch (currentDirection) {//signal and slot control direction
        case 0://down
            if(wall[this->GhostY + 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){//check turn able and turn at center of blocks
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveDown()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 1://up
            if(wall[this->GhostY - 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveUp()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 2://right
            if(wall[this->GhostY][this->GhostX + 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveRight()));
            }
            else {
                forceToChangeDirection();
            }
        break;
        case 3://left
            if(wall[this->GhostY][this->GhostX - 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveLeft()));
            }
            else {
                forceToChangeDirection();
            }
        break;
    }
}

void ghost::forceToChangeDirection(){//invoke when needed
    currentDirection = nextDirection;
    nextDirection = qrand() % 4;
}
//------slots------
void ghost::changeDirection(){//link with timer
    nextDirection = qrand() % 4;
}

void ghost::moveUp(){
    if(wall[this->GhostY - 1][this->GhostX] == nullptr){
        this->setY(this->y() - 1);//up
        if(static_cast<int>(this->y() - 2) % 15 == 0){//reach edge
            this->GhostY = static_cast<int>(this->y() - 2)/15;
        }
        collision();
        checkNextDirection();
    }
}

void ghost::moveDown(){
    if(wall[this->GhostY + 1][this->GhostX] == nullptr){
        this->setY(this->y() + 1);//down
        if(static_cast<int>(this->y() - 2) % 15 == 0){
            this->GhostY = static_cast<int>(this->y() - 2)/15;
        }
        collision();
        checkNextDirection();
    }
}

void ghost::moveLeft(){
    if(wall[this->GhostY][this->GhostX - 1] == nullptr){
        this->setX(this->x() - 1);//left
        if(static_cast<int>(this->x() - 2) % 15 == 0){
            this->GhostX = static_cast<int>(this->x() - 2 )/15;
        }
        collision();
        checkNextDirection();
    }
}

void ghost::moveRight(){
    if(wall[this->GhostY][this->GhostX + 1] == nullptr){
        this->setX(this->x() + 1);//right
        if(static_cast<int>(this->x() - 2) % 15 == 0){
            this->GhostX = static_cast<int>(this->x() - 2)/15;
        }
        collision();
        checkNextDirection();
    }
}

void ghost::reviveGhost(){
    this->dead = false;
    setPos(2 + originalX * 15,2 + originalY * 15);
    this->GhostX = originalX;
    this->GhostY = originalY;
    this->setVisible(true);
    revive->stop();
}
//------slots------
void ghost::checkNextDirection(){//move toward next direction when available
    switch (nextDirection) {
    case 0://down
        if(wall[this->GhostY + 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){//check turn able and turn at center of blocks
            currentDirection = nextDirection;
            nextDirection = qrand() % 4;
        }
    break;
    case 1://up
        if(wall[this->GhostY - 1][this->GhostX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){
            currentDirection = nextDirection;
            nextDirection = qrand() % 4;
        }
    break;
    case 2://right
        if(wall[this->GhostY][this->GhostX + 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
            currentDirection = nextDirection;
            nextDirection = qrand() % 4;
        }
    break;
    case 3://left
        if(wall[this->GhostY][this->GhostX - 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
            currentDirection = nextDirection;
            nextDirection = qrand() % 4;
        }
    break;
    }
}
void ghost::collision(){
    if(this->collidesWithItem(pac)){
        if(pac->angry == true){//kill ghost
           this->dead = true;
           this->setVisible(false);//hide the ghost
            //move on the wall
           this->setPos(2,2);
           this->GhostX = 0;
           this->GhostY = 0;
           disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);//stop moving
           ++(*deadGhost);//for computing score
           revive->start(10000);//revive ghost after 10 secs
        }
        else {
            *deadPacman = 1;//end game
        }
    }
}
