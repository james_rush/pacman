#ifndef PACMAN_H
#define PACMAN_H

#include "moveableitem.h"
#include <QTimer>

class pacman : public moveableitem
{
    Q_OBJECT
public:
    pacman(QGraphicsItem *parent = nullptr,int x = 0, int y = 0,QString picPath = " ",QGraphicsRectItem *w[31][28] = nullptr,QGraphicsEllipseItem *d[31][28] = nullptr,QGraphicsScene *s = nullptr,int * dotsN = nullptr);
    void move(int direction);
    ~pacman();
    void checkTeleport();
    void eatDot();
    void angryPower();
    bool angry = false;//power pill
private:
    QTimer *tmr,*angryTime;
    QGraphicsRectItem *wall[31][28];
    QGraphicsEllipseItem *dot[31][28];
    QGraphicsScene *scene;
    int PacX = 0,PacY = 0;
    int *dotNum;//dot num as pointer
private slots:
    void moveUp();
    void moveDown();
    void moveRight();
    void moveLeft();
    void recover();
};
#endif // PACMAN_H
