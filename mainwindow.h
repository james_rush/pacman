#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include "pacman.h"
#include "ghost.h"
#include "moveableitem.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void readMap();
    void drawMap();
    void setMovingObject();
private:
    QGraphicsRectItem *wall[31][28];
    QGraphicsEllipseItem *dot[31][28];
    int map[31][28];
    int dotNum = 0,deadGhost = 0,deadPacman = 0;
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    pacman *pac;
    ghost *pink,*yellow,*red,*blue;
    QTimer *updateScore,*moveGhostTmr;
private slots:
    void scoreBoard();
    void moveGhost();
    // QWidget interface
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H
