#ifndef MOVEABLEITEM_H
#define MOVEABLEITEM_H

#include <QGraphicsPixmapItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsRectItem>
#include <QGraphicsView>



class moveableitem:public QObject,public QGraphicsPixmapItem
{
    //Q_OBJECT
public:
    explicit moveableitem(QGraphicsItem *parent = nullptr);
    virtual void move(int direction) = 0;
    ~moveableitem();
};

#endif // MOVEABLEITEM_H
