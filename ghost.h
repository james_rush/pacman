#ifndef GHOST_H
#define GHOST_H

#include "moveableitem.h"
#include "pacman.h"

class ghost:public moveableitem
{
    Q_OBJECT
public:
    explicit ghost(QGraphicsItem *parent = nullptr,int x = 0, int y = 0,QString picPath = " ",QGraphicsRectItem *w[31][28] = nullptr,int *deadG = nullptr,int* deadP= nullptr,pacman *pac = nullptr);
    void move(int mode);
    ~ghost();
    void movePatternA();
    void movePatternB();
    void movePatternC();
    void movePatternD();
    void forceToChangeDirection();
    void checkNextDirection();
    void collision();
private:
    QTimer *tmr,*dir,*revive;
    QGraphicsRectItem *wall[31][28];
    QGraphicsScene *scene;
    pacman *pac;
    bool dead = false;
    int currentDirection = 4,nextDirection;
    int GhostX = 0,GhostY = 0,originalX = 0,originalY = 0;
    int *deadGhost,*deadPacman;//deadGhost pointer for computing score,deadPacman for end game
private slots:
    void moveUp();
    void moveDown();
    void moveRight();
    void moveLeft();
    void changeDirection();
    void reviveGhost();
};
#endif // GHOST_H
