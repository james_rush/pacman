#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "pacman.h"
#include "ghost.h"
#include "moveableitem.h"
#include <QFile>
#include <QtDebug>
#include <QGraphicsItem>
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene;
    updateScore = new QTimer;//update scoreboard every 0.2 sec
    updateScore->start(200);
    connect(updateScore, SIGNAL(timeout()),this,SLOT(scoreBoard()));
    ui->end->setVisible(false);//hide ending sign
    moveGhostTmr = new QTimer;
    moveGhostTmr->start(20);
    connect(moveGhostTmr, SIGNAL(timeout()),this,SLOT(moveGhost()));

    readMap();
    drawMap();
    setMovingObject();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readMap(){//read map in
    QString path = ":/map.txt";
    QFile file(path);
    QTextStream in(&file);
    QString trash,line;
    QStringList list;
    QDebug dbg(QtDebugMsg);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug()<<"file open failed"<<endl;
    }
    for (int i = 0;i < 31;++i) {
            line = in.readLine();
            list = line.split(",");
        for (int j = 0;j < 28;++j) {
            map[i][j] = list.at(j).toInt();
        }
    }
    file.close();
}

void MainWindow::drawMap(){
    for (int i = 0;i < 31;++i) {
        for (int j = 0;j < 28;++j) {
            if(map[i][j] == 0){//wall
                wall[i][j] = new QGraphicsRectItem(15 * j,15 *i,15,15);
                wall[i][j]->setBrush(Qt::blue);
                scene->addItem(wall[i][j]);
            }
            else if (map[i][j] == 2) {//power pill
                 dot[i][j] = new QGraphicsEllipseItem(2 + j*15,2 + i*15,10,10);
                 dot[i][j]->setBrush(Qt::yellow);
                 scene->addItem(dot[i][j]);
                 dotNum++;
            }
            else{//dots
                dot[i][j] = new QGraphicsEllipseItem(5 + j*15,5 + i*15,5,5);
                dot[i][j]->setBrush(Qt::white);
                scene->addItem(dot[i][j]);
                dotNum++;
            }
        }
    }
    ui->graphicsView->setScene(scene);
}

void MainWindow::setMovingObject(){
    pac = new pacman(nullptr,1,1,":/pic/pic/pacman.png",wall,dot,scene,&dotNum);
    pink = new ghost(nullptr,12,14,":/pic/pic/pinkghost.png",wall,&deadGhost,&deadPacman,pac);
    yellow = new ghost(nullptr,13,14,":/pic/pic/yellowghost.png",wall,&deadGhost,&deadPacman,pac);
    red = new ghost(nullptr,14,14,":/pic/pic/redgohst.png",wall,&deadGhost,&deadPacman,pac);
    blue = new ghost(nullptr,15,14,":/pic/pic/blueghost.png",wall,&deadGhost,&deadPacman,pac);
    scene->addItem(pac);
    scene->addItem(pink);
    scene->addItem(blue);
    scene->addItem(red);
    scene->addItem(yellow);
    ui->graphicsView->setScene(scene);
}


void MainWindow::keyPressEvent(QKeyEvent *event)
{
    moveableitem *m;
    m = pac;

    if(event->key() == Qt::Key_W){
        m->move(1);
    }
    else if(event->key() == Qt::Key_A){
        m->move(3);
    }
    else if(event->key() == Qt::Key_S){
        m->move(0);
    }
    else if(event->key() == Qt::Key_D){
        m->move(2);
    }
}

void MainWindow::scoreBoard(){
    ui->Score->setNum((316 - dotNum) * 50 + deadGhost * 250);
    if(dotNum == 0){//win game
        disconnect(moveGhostTmr,SIGNAL(timeout()),nullptr,nullptr);//disconnect timer link with ghost that will be cleared later
        scene->clear();
        ui->end->setVisible(true);
    }
    if(deadPacman == 1){//lost game
        disconnect(moveGhostTmr,SIGNAL(timeout()),nullptr,nullptr);//disconnect timer link with ghost that will be cleared later
        scene->clear();
        ui->end->setText("You Lost");
        ui->end->setStyleSheet("color:Red");
        ui->end->setVisible(true);
    }
}

void MainWindow::moveGhost(){
    moveableitem *m;
    m = red;//link different ghost for polymorphsim
    m->move(0);
    m = pink;
    m->move(1);
    m = blue;
    m->move(2);
    m = yellow;
    m->move(3);
}
