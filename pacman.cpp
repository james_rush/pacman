#include "pacman.h"
#include "moveableitem.h"

#include <QGraphicsScene>

pacman::pacman(QGraphicsItem* parent,int x,int y,QString picpath,QGraphicsRectItem *w[31][28],QGraphicsEllipseItem *d[31][28],QGraphicsScene *s,int* dotsN):moveableitem(parent){
    this->setPixmap(QPixmap(picpath).scaled(QSize(13,13)));
    this->setPos(2 + 15 * x,2 + 15 * y);//set pacman pos
    this->PacX = x;
    this->PacY = y;
    for (int i = 0;i < 31;++i) {//copy dot and wall array
        for(int j = 0;j < 28 ;++j){
            this->wall[i][j] = w[i][j];
            this->dot[i][j] = d[i][j];
        }

    }
    this->scene = s;//copy graphicscene
    this->tmr = new QTimer;
    tmr->start(20);
    this->angryTime = new QTimer;
    this->dotNum = dotsN;
    connect(angryTime,SIGNAL(timeout()),this,SLOT(recover()));
}
pacman::~pacman(){

}

void pacman::move(int direction){
    switch (direction) {//signal and slot control direction
        case 0://down
            if(wall[this->PacY + 1][this->PacX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){//check turn able and turn at center of blocks
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveDown()));
            }
        break;
        case 1://up
            if(wall[this->PacY - 1][this->PacX] == nullptr && static_cast<int>(this->x() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveUp()));
            }
        break;
        case 2://right
            if(wall[this->PacY][this->PacX + 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveRight()));
            }
        break;
        case 3://left
            if(wall[this->PacY][this->PacX - 1] == nullptr && static_cast<int>(this->y() - 2) % 15 == 0){
                disconnect(tmr,SIGNAL(timeout()),nullptr,nullptr);
                connect(tmr, SIGNAL(timeout()), this, SLOT(moveLeft()));
            }
        break;
    }
}

void pacman::moveUp(){
    if(wall[this->PacY - 1][this->PacX] == nullptr){
        this->setY(this->y() - 1);//up
        if(static_cast<int>(this->y() - 2) % 15 == 0){//reach edge
            this->PacY = static_cast<int>(this->y() - 2)/15;
        }
        eatDot();
    }
}

void pacman::moveDown(){
    if(wall[this->PacY + 1][this->PacX] == nullptr){
        this->setY(this->y() + 1);//down
        if(static_cast<int>(this->y() - 2) % 15 == 0){
            this->PacY = static_cast<int>(this->y() - 2)/15;
        }
        eatDot();
    }
}

void pacman::moveLeft(){
    if(wall[this->PacY][this->PacX - 1] == nullptr){
        this->setX(this->x() - 1);//left
        if(static_cast<int>(this->x() - 2) % 15 == 0){
            this->PacX = static_cast<int>(this->x() - 2 )/15;
        }
        eatDot();
        checkTeleport();
    }
}

void pacman::moveRight(){
    if(wall[this->PacY][this->PacX + 1] == nullptr){
        this->setX(this->x() + 1);//right
        if(static_cast<int>(this->x() - 2) % 15 == 0){
            this->PacX = static_cast<int>(this->x() - 2)/15;
        }
        eatDot();
        checkTeleport();
    }
}

void pacman::checkTeleport(){//teleport at the end of road
    if(this->PacX == 0 && this->PacY == 14){
        this->setX(2 + 26 * 15);
        this->PacX = 26;//refresh pacx
    }
    else if (this->PacX == 27 && this->PacY == 14) {
        this->setX(2 + 1 * 15);
        this->PacX = 1;//refresh pacx
    }
}

void pacman::eatDot(){
    if(dot[PacY][PacX] != nullptr){
        if(PacX == 1 && PacY == 8){//power pill position
            angryPower();
        }
        else if (PacX == 1 && PacY == 26) {
            angryPower();
        }
        else if (PacX == 26 && PacY == 8) {
            angryPower();
        }
        else if (PacX == 26 && PacY == 26) {
            angryPower();
        }
        else if (PacX == 14 && PacY == 11) {
            angryPower();
        }
        else if (PacX == 13 && PacY == 17) {
            angryPower();
        }

        scene->removeItem(dot[PacY][PacX]);//clear dot
        dot[PacY][PacX] = nullptr; //clear pointer
        (*dotNum) --;
    }
}

void pacman::angryPower(){
    this->angry = true;
    this->setPixmap(QPixmap(":/pic/pic/angrypacman.png").scaled(QSize(13,13)));
    this->angryTime->start(10000);//be angry for ten seconds
}

void pacman::recover(){
    this->angry = false;
    this->setPixmap(QPixmap(":/pic/pic/pacman.png").scaled(QSize(13,13)));
    this->angryTime->stop();
}
